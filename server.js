'use strict';

const hapi = require('hapi'),
	child = require('child_process'),
	{ resolve } = require('path'),
	fs = require('fs'),
	config = require('./config');

global.appRoot = resolve(__dirname);

const server = new hapi.Server({
	port: config.port,
	routes: {cors: true}
});

module.exports = server;
global.server = server;

server.ext('onPreResponse', (request, h) => {
	const response = request.response;
	if (response.isBoom) {
		const is4xx = response.output.statusCode >= 400 && response.output.statusCode < 500;
		if (is4xx && response.data)
			response.output.payload.data = response.data;
	}
	return h.continue;
});

server.ext('onPreResponse', (request, h) => {
	const response = request.response;
	if(request.method !== 'get' && response.isBoom){
		try {
			fs.unlinkSync(request.payload.path);
		} catch (e) {
			console.log(e);
		}
	}
	return h.continue;
});


let plugins = [
	require('inert'),
	require('vision'), {
		plugin: require('good'),
		options: {
			ops: {
				interval: 1000
			},
			reporters: {
				console: [{
					module: 'good-squeeze',
					name: 'Squeeze',
					args: [{
						log: '*',
						response: '*',
						request: '*'
					}]
				}, {
					module: 'good-console'
				}, 'stdout']
			}
		}
	}, {
		plugin: require('hapi-swagger'),
		options: {
			host: config.domain + ':' + config.port,
			info: {
				title: 'Fileservice API',
				description: 'Powered by node, hapi, joi, hapi-swaggered, hapi-swaggered-ui and swagger-ui',
				version: '0.1.0'
			}
		}
	}
];

async function init() {
	await server.register(plugins);
	require('./routes.js')(server);

	await server.start();
	server.log('info', 'Server started at ' + server.info.uri);
	child.execSync('mkdir -p ' + config.fsPath + '/images');
	child.execSync('mkdir -p ' + config.fsPath + '/audios');
	child.execSync('mkdir -p ' + config.fsPath + '/videos');
	server.log('info', 'Using ' + require('os').tmpdir() + ' as tmp dir');
}

process.on('unhandledRejection', (err) => {
	console.error(err);
	process.exit(1);
});

init();
