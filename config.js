'use strict';

const { isEmpty } = require('lodash');

const domain = (!isEmpty(process.env.VIRTUAL_HOST)) ? process.env.VIRTUAL_HOST : 'localhost';
const port = (!isEmpty(process.env.VIRTUAL_PORT)) ? process.env.VIRTUAL_PORT : '3000';
const protocol = 'http';
const hostname = protocol + '://' + domain + ((port === '80') ? '' : ':' + port);
console.info('Using ' + hostname + ' as hostname');

let fsPath = require('path').dirname(__filename);
if (!isEmpty(process.env.APPLICATION_PATH)) {
	fsPath = process.env.APPLICATION_PATH;
}
fsPath = fsPath.endsWith('/') ? fsPath : fsPath + '/';

module.exports = {
	domain: domain,
	port: port,
	protocol: protocol,
	hostname: hostname,
	fsPath: fsPath,
	tmp: require('os').tmpdir()
};
