'use strict';

const Joi = require('joi'),
	handlers = require('./controllers/handler'),
	conf = require('./config');

module.exports = function(server) {

	server.route({
		method: 'GET',
		path: '/image/{filename*}',
		handler: {
			directory: {
				path: conf.fsPath + 'images/'
			}
		},
		config: {
			validate: {
				params: {
					filename: Joi.string().trim().required()
				},
			},
			plugins: {
				'hapi-swagger': {
					produces: ['image/jpeg', 'image/png'],
					responses: {
						' 200 ': {
							'description': 'A pictue is provided'
						},
						' 400 ': {
							'description': 'Probably a parameter is missing or not allowed'
						},
						' 404 ': {
							'description': 'No image was found'
						}
					}
				}
			},
			tags: ['api'],
			description: 'Get an image by filename'
		}
	});

	server.route({
		method: 'GET',
		path: '/videos/{filename*}',
		handler: {
			directory: {
				path: conf.fsPath + 'videos/'
			}
		},
		config: {
			validate: {
				params: {
					filename: Joi.string().trim().required()
				},
			},
			plugins: {
				'hapi-swagger': {
					produces: ['video/mp4'],
					responses: {
						' 200 ': {
							'description': 'A video is provided'
						},
						' 400 ': {
							'description': 'Probably a parameter is missing or not allowed'
						},
						' 404 ': {
							'description': 'No video was found'
						}
					}
				}
			},
			tags: ['api'],
			description: 'Get a video by filename'
		}
	});

	server.route({
		method: 'GET',
		path: '/audios/{filename*}',
		handler: {
			directory: {
				path: conf.fsPath + 'audios/'
			}
		},
		config: {
			validate: {
				params: {
					filename: Joi.string().trim().required()
				},
			},
			plugins: {
				'hapi-swagger': {
					produces: ['music/mp3'],
					responses: {
						' 200 ': {
							'description': 'An audio track is provided'
						},
						' 400 ': {
							'description': 'Probably a parameter is missing or not allowed'
						},
						' 404 ': {
							'description': 'No audio track was found'
						}
					}
				}
			},
			tags: ['api'],
			description: 'Get an audio track by filename'
		}
	});

	server.route({
		method: 'PUT',
		path: '/image',
		handler: handlers.storePicture,
		config: {
			payload: {
				output: 'file',
				maxBytes: 10485760, //10MB
				failAction: 'log'
			},
			validate: {
				payload: Joi.required(),
				failAction: handlers.failedValidation
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['image/*'],
					responses: {
						' 200 ': {
							'description': 'Successfully uploaded and stored an image, see response',
						},
						' 400 ': {
							'description': 'Probably a parameter is missing or not allowed'
						}
					}
				}
			},
			tags: ['api'],
			description: 'Store an image'
		},
	});

	server.route({
		method: 'PUT',
		path: '/audio',
		handler: handlers.storeAudio,
		config: {
			payload: {
				output: 'file',
				maxBytes: 52428800, //50MB
				failAction: 'log'
			},
			validate: {
				payload: Joi.required(),
				failAction: handlers.failedValidation
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['audio/*'],
					responses: {
						' 200 ': {
							'description': 'Successfully uploaded and stored an audio track, see response',
						},
						' 400 ': {
							'description': 'Probably a parameter is missing or not allowed'
						}
					}
				}
			},
			tags: ['api'],
			description: 'Store an audio track'
		},
	});

	server.route({
		method: 'PUT',
		path: '/video',
		handler: handlers.storeVideo,
		config: {
			payload: {
				output: 'file',
				maxBytes: 10485760, //100MB
				failAction: 'log'
			},
			validate: {
				payload: Joi.required(),
				failAction: handlers.failedValidation
			},
			plugins: {
				'hapi-swagger': {
					consumes: ['video/*'],
					responses: {
						' 200 ': {
							'description': 'Successfully uploaded and stored a video, see response',
						},
						' 400 ': {
							'description': 'Probably a parameter is missing or not allowed'
						}
					}
				}
			},
			tags: ['api'],
			description: 'Store a video'
		},
	});

};
