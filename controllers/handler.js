'use strict';

const boom = require('boom'),
	conf = require('../config'),
	fs = require('fs'),
	hasha = require('hasha'),
	{ isEmpty } = require('lodash'),
	readChunk = require('read-chunk'),
	fileType = require('file-type'),
	is_svg = require('is-svg');

module.exports = {

	failedValidation: async function (request, h, err) {
		try {
			fs.unlinkSync(request.payload.path);
		} catch (e) {
			console.log(e);
		}
		if (err.isJoi) {
			return h.continue;
		} else
			return boom.badImplementation();

	},

	storePicture: async function(request, h) {
		const result = testPayload(request);
		if(result !== 'ok')
			return result;
		else
			return saveFile(request, 'image');
	},

	storeAudio: async function(request, h) {
		const result = testPayload(request);
		if(result !== 'ok')
			return result;
		else
			return saveFile(request, 'audio');
	},

	storeVideo: async function(request, h) {
		const result = testPayload(request);
		if(result !== 'ok')
			return result;
		else
			return saveFile(request, 'video');
	}
};

function testPayload(request) {
	if(isEmpty(request.payload)){
		return boom.entityTooLarge('Seems like the payload was to large - 10MB max');
	} else if (request.payload.bytes <= 1) { //no payload
		fs.unlinkSync(request.payload.path);
		return boom.badRequest('A payload is required');
	} else
		return 'ok';
}

async function saveFile(request, type) {
	try {
		let buffer = readChunk.sync(request.payload.path, 0, fileType.minimumBytes);
		const readFileType = fileType(buffer);
		if(isEmpty(readFileType) || (!isEmpty(readFileType) && !readFileType.mime.startsWith(type))){
			if(type === 'image'){
				const fileContent = fs.readFileSync(request.payload.path, {encoding: 'utf8'});
				if(!is_svg(fileContent))
					return boom.unsupportedMediaType();
			} else
				return boom.unsupportedMediaType();
		}
		let fileExtension;
		if(!isEmpty(type))
			fileExtension = readFileType.ext;
		else
			fileExtension = 'svg';
		let sum = await hasha.fromFile(request.payload.path, {algorithm: 'sha256'});
		fs.copyFileSync(request.payload.path, conf.fsPath + type + 's/'  + sum + '.' + fileExtension);
		fs.unlinkSync(request.payload.path);
		return {filename: sum + '.' + fileExtension};
	} catch (err) {
		request.log(err);
		return boom.badImplementation();
	}
}
