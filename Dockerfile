FROM node:alpine
LABEL maintainer="Roy Meissner <meissner@informatik.uni-leipzig.de>"

ARG BUILD_ENV=local
ENV VIRTUAL_PORT=${VIRTUAL_PORT:-80}

RUN mkdir /nodeApp
WORKDIR /nodeApp

# --------------------------- #
#   Installation Custom App   #
# --------------------------- #

#RUN apt-get update && apt-get install -y imagemagick coreutils bzip2 libfontconfig libfreetype6

COPY ./ ./

RUN if [ "$BUILD_ENV" = "CI" ] ; then npm prune --production ; else rm -R node_modules ; npm install --production ; fi

# ----------------- #
#   Configuration   #
# ----------------- #

EXPOSE 80
VOLUME /data/files

# ----------- #
#   Cleanup   #
# ----------- #

#RUN apt-get autoremove -y && apt-get -y clean && rm -rf /var/lib/apt/lists/*

# -------- #
#   Run!   #
# -------- #

CMD ["npm","start"]
