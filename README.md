# EAs.LiT File Service

This service provides a REST-API to store and serve media files (pictures, audio, videos).

## How to get started
1. Clone the repository via `git clone ...`.
2. Change into the directory and install all dependencies via `npm install`
3. Transpile and run the project via `npm start`. The started service is accessible via [http://localhost:3000](http://localhost:300)

The service is now started in development mode. You may run `npm run lint` to lint all code via ESLint.

See the docker-compose.yml files for configuration options.

## Production Environment
Use the included dockerfile to build a docker image and the included docker-compose.yml files to start it up.

Look at the dockerfile and docker-compose.yml files to gain insights into manual deployment.

## Automatic Builds and Deployments
By including `[build]` to a commit message on the main branch, a GitLab CI pipeline is triggered and builds a docker image, which is published on GitLab. Afterwards, a deployment stage is triggered, which deploys this newly built image to the staging environment. Deployment may also be triggered on its own, by including `[deploy]` to a commit message.

## Development
The root directory contains all source code, as well as all configuration files. The easiest way to get around is to look at the routes.js file.

### Preliminaries ###

We assume an ext4 filesystem or at least a filesystem with no restriction for the number of files per folder.
The service uses /tmp as a directory for temporary files. It can be speeded up massivly by having /tmp as a directory that lives in RAM. See [Tmpfs](https://wiki.archlinux.org/index.php/Tmpfs) for imformation about filesystem transfer into RAM.
